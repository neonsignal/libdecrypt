// define a set of codepoints
// Copyright 2022 Glenn McIntosh
// licensed under the GNU General Public Licence version 3

#pragma once
#include <cstdint>
#include <vector>
#include <string>

class alphabet
{
public:
	// define alphabet
	alphabet(const std::string subset)
	: length(ssize(subset)), from_char(256, 255), to_char(256, '#')
	{
		for (int i = 0; i < ssize(subset); ++i)
		{
			from_char[subset[i]] = i;
			to_char[i] = subset[i];
		}
	}

	// size of alphabet
	int size() const
	{
		return this->length;
	}

	// conversion from character to codepoint
	uint8_t from(char c) const
	{
		return from_char[c];
	}

	// conversion to character from codepoint
	char to(uint8_t d) const
	{
		return to_char[d];
	}
private:
	int length;
	std::vector<uint8_t> from_char;
	std::vector<char> to_char;
};

class codepoints
{
public:
	// convert from string to codepoints
	codepoints(const std::string value, const alphabet a)
	{
		for (char c: value)
			data.emplace_back(a.from(c));
	}

	// preallocate space
	codepoints(int length)
	: data(length, 255)
	{
	}

	// convert from codepoints to string
	std::string display(const alphabet a) const
	{
		using std::string;
		string s;
		for (uint8_t d: data)
			s.push_back(a.to(d));
		return s;
	}

	// size of codepoint vector
	int size() const
	{
		return data.size();
	}

	// element of codepoints
	const uint8_t &operator[](int i) const
	{
		return data[i];
	}
	uint8_t &operator[](int i)
	{
		return data[i];
	}

	// index of coincidence
	double kappa() const;
private:
	std::vector<uint8_t> data;
};
