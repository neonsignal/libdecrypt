// Copyright 2020-2022 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// linear annealing

#pragma once
#include <cmath>

template <typename T> class Anneal
{
public:
	// initialize with alphabet
	Anneal(const T &alphabet) : alphabet(alphabet) {}

	// place back a new alphabet depending on fitness measure
	// temperature from 1 to 0
	bool emplace_back(const T alphabet, float fit, float temperature)
	{
		if (exp((fit-fit_best)/temperature) < rand()%65536/65536.)
			return false;
		fit_best = fit;
		this->alphabet = alphabet;
		return true;
	}

	// select a new alphabet
	T select()
	{
		return alphabet;
	}
public:
	float fit_best{0};
	T alphabet;
};
