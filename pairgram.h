// ngram fit calculations
#include <fstream>
#include <string>
#include "codepoints.h"
#include <array>

class Pairgram
{
	static constexpr int N{26};
	static constexpr int M{8};
public:
	Pairgram(const std::string &filename)
	{
		using std::ifstream;
		ifstream file(filename);
		file.read((char *) f, sizeof(f));
		file.close();
	}

	// calculate weighted ngram sum
	float fit_weight(const codepoints &lookup)
	{
		const int n{lookup.size()};
		int fit = 0;
		for (int i = 0; i < n; ++i)
		for (int p = 0; p < M && i+1+p < n; ++p)
		{
			int c0 = lookup[i];
			int c1 = lookup[i+1+p];
			fit += f[p][c0][c1];
		}
		return 1.*fit/M/n;
	}

private:
	uint8_t f[M][N][N];
};

