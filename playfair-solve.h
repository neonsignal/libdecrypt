// playfair decoder
// Copyright 2022 Glenn McIntosh
// licensed under the GNU General Public Licence version 3

#include "codepoints.h"
#include "wordnet.h"
#include <string>
#include <vector>
#include <iostream>

class PlayfairSolver
{
public:
	// construct playfair solver
	PlayfairSolver(std::istream &dictionary);

	// solve ciphertext
	void operator()(const codepoints &cipher, std::vector<codepoints> &solutions);
private:
	// recursive solution
	void solve(const codepoints &cipher, int i, const Wordnet &w, int i_select, uint32_t location_mask);
	void check_cipher(const codepoints &cipher, int i, uint8_t plain, const Wordnet &w, int i_select, uint32_t location_mask);
	void check_plain(const codepoints &cipher, int i, uint8_t plain, const Wordnet &w, int i_select, uint32_t location_mask);
private:
	Wordnet wordnet;
	const std::string lc{"abcdefghiklmnopqrstuvwxyz"};
	static constexpr uint8_t dummy{25};  // marker between double letters

	std::unique_ptr<codepoints> p_solution;
	std::vector<codepoints> *p_solutions;

	uint32_t letter_masks[25]{};

	struct selector
	{
		uint32_t mask;
		int next[25]{};
	};
	std::vector<selector> selectors;
};
