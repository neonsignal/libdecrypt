// generate ngrams from all word combinations from a dictionary
#include <math.h>
#include <stdint.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <set>
typedef unsigned int uint;
const size_t N = 26;
int main()
{
// initialize ngram array
	static uint8_t ngram[N][N][N][N][N];
	for (size_t i0 = 0; i0 < N; ++i0)
	for (size_t i1 = 0; i1 < N; ++i1)
	for (size_t i2 = 0; i2 < N; ++i2)
	for (size_t i3 = 0; i3 < N; ++i3)
	for (size_t i4 = 0; i4 < N; ++i4)
		ngram[i0][i1][i2][i3][i4] = 0;

	// for each dictionary line
	std::cerr << "read dictionary" << std::endl;
	std::vector<std::string> dictionary;
	while (!std::cin.eof())
	{
		std::string word;
		std::cin >> word;
		dictionary.emplace_back(word);
	}

	// create context table
	std::cerr << "create context" << std::endl;
	std::set<std::string> contextset;
	for (int i0 = 0; i0 < dictionary.size(); ++i0)
	{
		std::string word = dictionary[i0];
		if (word.size() < 4)
			for (int i1 = 0; i1 < dictionary.size(); ++i1)
			{
				std::string word = dictionary[i0]+dictionary[i1];
				if (word.size() < 4)
					continue;
				contextset.insert(word.substr(0,4));
			}
		else
			contextset.insert(word.substr(0,4));
	}

	// calculate ngrams
	std::cerr << "calculate ngrams" << std::endl;
	for (auto it = contextset.begin(); it != contextset.end(); ++it)
		for (int i = 0; i < dictionary.size(); ++i)
		{
			size_t n = dictionary[i].length();
			std::string word = dictionary[i] + *it;
			for (size_t i = 0; i < n; ++i)
			{
				size_t i0 = word[i+0]-'a';
				size_t i1 = word[i+1]-'a';
				size_t i2 = word[i+2]-'a';
				size_t i3 = word[i+3]-'a';
				size_t i4 = word[i+4]-'a';
				ngram[i0][i1][i2][i3][i4] = 1;
			}
		}

	// write out ngram table
	std::cerr << "write out ngrams" << std::endl;
	/*
	for (size_t i0 = 0; i0 < N; ++i0)
	for (size_t i1 = 0; i1 < N; ++i1)
	for (size_t i2 = 0; i2 < N; ++i2)
	for (size_t i3 = 0; i3 < N; ++i3)
	for (size_t i4 = 0; i4 < N; ++i4)
		if (ngram[i0][i1][i2][i3][i4])
			std::cout << char('a'+i0) << char('a'+i1) << char('a'+i2) << char('a'+i3) << char('a'+i4) << std::endl;
	*/
	{
		std::ofstream tableFile("ngram.bin");
		tableFile.write((char *) ngram, sizeof(ngram));
		tableFile.close();
	}

	// test
	{
		const char m[] = "vakxdtmoxatnzixzyzesyxtedpyfqeooxvvnlanypoyofhgzuwbxvrfafnwokxnastzscbeygzpdtotxcrlkioqkfiewyvtqrojfzxslfszqbqmxbnquruzwnrgcbzkdaysmvrfnbjgezbojgbsbelliiykfirfjwasjurjdvlnalhvhqrbebqgxwltsugvxnrbzgupglrsrkowbehrcdeydaliybbqwzporxdcmekvwyzhhmkgagemryvnepyka";
		size_t n = sizeof(m)-1;
		uint fit = 0;
		for (size_t i = 4; i < n; ++i)
			fit += ngram[m[i-4]-'a'][m[i-3]-'a'][m[i-2]-'a'][m[i-1]-'a'][m[i-0]-'a'];
		std::cerr << "test fit: " << fit << ' ' << n << std::endl;
	}
}
