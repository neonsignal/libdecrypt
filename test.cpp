// test decryption algorithms
// Copyright 2022 Glenn McIntosh
// licensed under the GNU General Public Licence version 3

#include "codepoints.h"
#include "ngram.h"
#include "pairgram.h"
#include "wordnet.h"
#include "anneal.h"
#include "transpose.h"
#include "substitute.h"
#include "playfair.h"
#include "playfair-solve.h"
#include "multipleanagram-solve.h"
#include "brute.h"
#include <iostream>
using std::cout, std::endl;
#include <fstream>
using std::ifstream;
#include <sstream>
using std::istringstream;
#include <string>
using std::string;
#include <algorithm>
using std::swap;
#include <memory>
using std::unique_ptr, std::make_unique;
#include <vector>
using std::vector;
#include "time.h"

int main()
{
	srand(time(NULL));

	cout << "*** transposition ***" << endl;
	{
		alphabet lc{"abcdefghijklmnopqrstuvwxyz"};
		alphabet uc{"ABCDEFGHIJKLMNOPQRSTUVWXYZ"};
		string ciphertext
		{
			"ENDYAHROHNLSRHEOCPTEOIBIDYSHNAIA"
			"CHTNREYULDSLLSLLNOHSNOSMRWXMNE"
			"TPRNGATIHNRARPESLNNELEBLPIIACAE"
			"WMTWNDITEENRAHCTENEUDRETNHAEOE"
			"TFOLSEDTIWENHAEIOYTEYQHEENCTAYCR"
			"EIFTBRSPAMHHEWENATAMATEGYEERLB"
			"TEEFOASFIOTUETUAEOTOARMAEERTNRTI"
			"BSEDDNIAAHTTMSTEWPIEROAGRIEWFEB"
			"AECTDDHILCEIHSITEGOEAOSDDRYDLORIT"
			"RKLMLEHAGTDHARDPNEOHMGFMFEUHE"
			"ECDMRIPFEIMEHNLSSTTRTVDOHW"
		};
		codepoints cipher{ciphertext, uc};
		cout << "κ = " << cipher.kappa() << endl;
		cout << transpose(cipher, 192, true).display(lc) << endl;
	}

	cout << "*** substitution ***" << endl;
	{
		alphabet lc{"abcdefghijklmnopqrstuvwxyz"};
		alphabet uc{"ABCDEFGHIJKLMNOPQRSTUVWXYZ"};
		string ciphertext
		{
			"PCQVMJYPDLBYKLYSOKBXBJXWXV"
			"BXVZCJPOEYPDKBXBJYUXJLBJOO"
			"KCPKCPLBOLBCMKXPVXPVIYJKLP"
			"YDBLQBOPKBOBXVOPVOVLBOLXRO"
			"CISXXJMIKBOJCKOXPVEYKKOVLB"
			"ODJCMPVZOICJOBYSKXUYPDDJOX"
			"LEYPDICJXLBCMKXPVXPVCPOPYD"
			"BLKYBXNOZOOPJOACMPLYPDLCUC"
			"MLBOIXZROKCIFXKLXDOKXPVLBO"
			"RODOPVKCIXPAYOPLEYPDKSXUYS"
			"XEOKCZCRVXKLCAJXNOXIXNCMJC"
			"IUCMJSXGOKLU"
		};

		// ngram table
		unique_ptr<Ngram> ngram{make_unique<Ngram>("ngram.bin")};

		// initialize annealling population
		codepoints cipher{ciphertext, uc};
		Anneal anneal(codepoints("abcdefghijklmnopqrstuvwxyz", lc));

		// for multiple generations
		for (float temperature = 1.f; temperature > 0;)
		{
			// copy to local
			auto key = anneal.select();
			int i = rand()%26, j = (i+rand()%25)%26;
			swap(key[i], key[j]);

			// test fit
			float fit = ngram->fit_weight(monoalphabetic(cipher, key));

			// if better than best, add to population
			if (!anneal.emplace_back(key, fit, temperature))
				temperature -= 0.00001;
		}
		auto key = anneal.select();
		auto plaintext = monoalphabetic(cipher, key);
		float fit = ngram->fit_weight(plaintext);
		cout << fit << ' ';
		cout << plaintext.display(lc);
		cout << ' ' << key.display(lc) << endl;
	}

	cout << "*** wordnet ***" << endl;
	{
		// read in dictionary
		alphabet lc{"abcdefghijklmnopqrstuvwxyz"};
		Wordnet wordnet;
		ifstream dictionary("dictionary.txt");
		for (string text{}; getline(dictionary, text);)
			wordnet.add(codepoints(text, lc));

		// test wordnet
		for (auto &s1: wordnet)
			if (s1.code == 'q'-'a')
				for (auto &s2: *(s1.next))
					cout << char(s1.code+'a') << char(s2.code+'a') << endl;
	}

	cout << "*** vignere ***" << endl;
	{
		alphabet lc{"abcdefghijklmnopqrstuvwxyz"};
		alphabet uc{"ABCDEFGHIJKLMNOPQRSTUVWXYZ"};
		string ciphertext
		{
			"VVQ ECILMYMPM RVCOG UI LHOMNIDES KFCH KDF WASPTF US TFCFSTO "
			"ABXC BJX AZJKHMGJSIIMIVBCEQ QB NDEL UEISU HT KFG AUHD EGH OPCM "
			"MFS UVAJWH XRYMCOCI YU DDDXTMPT IU ICJQKPXT ES VVJAU MVRR TWHTC "
			"ABXC IU EOIEG O RDCGX EN UCR PV NTIPTYXEC RQVARIYYB RGZQ RSPZ "
			"RKSJCPH PTAX RSP EKEZ RAECDSTRZPT MZMSEB ACGG NSFQVVF MC KFG "
			"SMHE FTRF WH MVV KKGE PYH FEFM CKFRLISYTYXL XJ JTBBX RQ HTXD "
			"WBHZ AWVV FD ACGG AVXWZVV YCIAG OE NZY FET LGXA SCUH."
		};
		string keytext
		{
			"completevictory"
		};

		codepoints cipher{ciphertext, uc};
		codepoints key{keytext, lc};
		cout << vignere(cipher, key, uc).display(lc) << endl;

	}

	cout << "*** playfair ***" << endl;
	{
		string ciphertext
		{
			"KXJEYUREBEZWEHEWRYTU"
			"HEYFSKREHEGOYFJWTTTU"
			"OLKSYCAJPOBOTEJZONTX"
			"BYBNTGONEYCUZWRGDSON"
			"SXBOUYWRHEBAAHYUSEDQ"
		};

		const string uc{"ABCDEFGHJKLMNOPQRSTUVWXYZ"};
		const string lc{"abcdefghiklmnopqrstuvwxyz"};
		codepoints cipher{ciphertext, uc};
		codepoints key("royalnewzealandnavy", lc);

		Playfair playfair;
		playfair.set_key(key);
		cout << playfair(cipher).display(lc) << endl;
	}

	cout << "*** playfair solver ***" << endl;
	{
		string ciphertext
		{
			"CBFTMHGRIOTSTAUFSBDNWGNISBRVEFBQTABQRPEFBKSDGMNRPSRFBSUTTDMFEMABIM"
		};

		const string lc{"abcdefghiklmnopqrstuvwxyz"};
		const string uc{"ABCDEFGHIKLMNOPQRSTUVWXYZ"};
		codepoints cipher{ciphertext, uc};

		istringstream dictionary
		(
			"balm\n"
			"chief\n"
			"course\n"
			"feast\n"
			"great\n"
			"hurt\n"
			"in\n"
			"lifes\n"
			"minds\n"
			"natures\n"
			"nourisher\n"
			"of\n"
			"second\n"
		);
		vector<codepoints> solutions;
		PlayfairSolver solver(dictionary);
		solver(cipher, solutions);
		for (int i = 0; i < ssize(solutions); ++i)
			cout << solutions[i].display(lc) << endl;
	}

	cout << "*** multiple transposition solver ***" << endl;
	{
		string ciphertext
		{
			"WAHNEDNDFIOGRDTEYEWPITNRTEENRCSHSEHSAILNLTBHEYSBIEEAGUETTYHSYFBIREOLWD"
		};

		const string lc{"abcdefghijklmnopqrstuvwxyz"};
		const string uc{"ABCDEFGHIJKLMNOPQRSTUVWXYZ"};
		codepoints cipher{ciphertext, uc};

		istringstream dictionary
		(
			"and\n"
			"beautys\n"
			"besiege\n"
			"brow\n"
			"deep\n"
			"dig\n"
			"field\n"
			"forty\n"
			"in\n"
			"shall\n"
			"thy\n"
			"trenches\n"
			"when\n"
			"winters\n"
		);
		vector<codepoints> solutions;
		MultipleAnagramSolver solver(dictionary);
		solver(cipher, solutions);
		for (int i = 0; i < ssize(solutions); ++i)
		{
			auto result = solutions[i].display(lc);
			for (int j = 0; j < solutions[i].size(); j += 2)
				cout << result[j+0];
			cout << ' ';
			for (int j = 0; j < solutions[i].size(); j += 2)
				cout << result[j+1];
			cout << endl;
		}
	}

	cout << "*** brute force alternatives ***" << endl;
	{
		Brute_number<3> e0;
		Brute_number<2> e1;
		Brute_null e2;
		Brute_alternate e3(e2, e1);
		Brute_pair e4(e0, e3);
		do
		{
			cout << e0() << ' ';
			if (e3())
				cout << e1() << ' ';
			cout << endl;
		}
		while (!e4.advance());
	}

	return 0;
}
