// playfair decoder
// Copyright 2022 Glenn McIntosh
// licensed under the GNU General Public Licence version 3

#include "playfair-solve.h"
using std::ssize;
using std::string;
using std::istream;
using std::make_unique;
using std::vector;

// construct playfair solver
PlayfairSolver::PlayfairSolver(istream &dictionary)
{
	// initialize wordnet
	for (string text{}; getline(dictionary, text);)
	{
		for (char &c: text)
			if (c == 'j')
				c = 'i';
		wordnet.add(codepoints(text, lc));
	}

	// initialize mask selectors
	// for each plaintext pair
	for (int plain0 = 0; plain0 < 25; ++plain0)
		for (int plain1 = 0; plain1 < 25; ++plain1)
		{
			// calculate cipher locations
			int cipher0 = plain0-plain0%5; int cipher1 = plain1-plain1%5;
			if (plain0%5 == plain1%5 && plain0/5 != plain1/5)
			{
				cipher0 = (plain0+5)%25;
				cipher1 = (plain1+5)%25;
			}
			else if (plain0/5 == plain1/5 && plain0%5 != plain1%5)
			{
				cipher0 += (plain0+1)%5;
				cipher1 += (plain1+1)%5;
			}
			else
			{
				cipher0 += plain1%5;
				cipher1 += plain0%5;
			}

			// insert into selector table
			int i_start{0}, *p_index = &i_start;
			bool exists{true};
			int set[4] = {cipher0, plain0, cipher1, plain1};
			for (int i = 0; i < 4; ++i)
			{
				if (!exists)
					*p_index = selectors.size();
				int index = *p_index;
				if (index >= ssize(selectors))
					selectors.emplace_back(selector{});
				uint32_t mask = 1<<set[i];
				exists = selectors[index].mask & mask;
				selectors[index].mask |= mask;
				p_index = &selectors[index].next[set[i]];
			}
		}
}

// solve ciphertext
void PlayfairSolver::operator()(const codepoints &cipher, vector<codepoints> &solutions)
{
	p_solutions = &solutions;
	if (ssize(cipher)%2 || ssize(cipher) == 0)
		return;
	p_solution = make_unique<codepoints>(cipher.size());
	uint32_t location_mask{(1<<25)-1};
	location_mask ^= 1<<0;
	letter_masks[cipher[0]] = 1<<0;
	solve(cipher, 0, wordnet, 0, location_mask);
	letter_masks[cipher[0]] = 0;
}

// recursive solution
void PlayfairSolver::solve(const codepoints &cipher, int i, const Wordnet &w, int i_select, uint32_t location_mask)
{
	// check for solution
	if (i == ssize(cipher))
	{
		if (w.complete())
		{
			codepoints tableau(25);
			for (int j = 0; j < 25; ++j)
				if (letter_masks[j])
					tableau[__builtin_ctz(letter_masks[j])] = j;
			p_solutions->push_back(*p_solution);
			p_solutions->push_back(tableau);
		}
		return;
	}

	// if dummy was inserted previously, use deferred letter
	if (i > 0 && (*p_solution)[i-1] == dummy)
	{
		check_cipher(cipher, i, (*p_solution)[i-2], w, i_select, location_mask);
		return;
	}

	// possible trailing 'x'
	if (i == cipher.size()-1 && w.complete())
		check_cipher(cipher, i, 22, w, i_select, location_mask);

	// previous character
	uint8_t plain0 = dummy;
	if (i&1)
		plain0 = (*p_solution)[i-1];

	// for each word in wordnet
	if (w.complete())
		for (auto &s: wordnet)
			check_cipher(cipher, i, s.code==plain0 ? dummy : s.code, *(s.next), i_select, location_mask);

	// for each letter next in wordnet
	for (auto &s: w)
		check_cipher(cipher, i, s.code==plain0 ? dummy : s.code, *(s.next), i_select, location_mask);
}

void PlayfairSolver::check_cipher(const codepoints &cipher, int i, uint8_t plain, const Wordnet &w, int i_select, uint32_t location_mask)
{
	// for each cipher character location
	uint32_t letter_mask0 = letter_masks[cipher[i]];
	uint32_t mask = (letter_mask0 ? letter_mask0 : location_mask) & selectors[i_select].mask;
	while (mask)
	{
		uint32_t m = mask&-mask;
		mask ^= m;
		letter_masks[cipher[i]] = m;

		// next character in cipher
		int i_select1 = selectors[i_select].next[__builtin_ctz(m)];
		check_plain(cipher, i, plain, w, i_select1, location_mask & ~m);
	}
	letter_masks[cipher[i]] = letter_mask0;
}

void PlayfairSolver::check_plain(const codepoints &cipher, int i, uint8_t plain, const Wordnet &w, int i_select, uint32_t location_mask)
{
	// for each plain character location
	(*p_solution)[i] = plain;
	if (plain == dummy)
		plain = 22;
	uint32_t letter_mask0 = letter_masks[plain];
	uint32_t mask = (letter_mask0 ? letter_mask0 : location_mask) & selectors[i_select].mask;
	while (mask)
	{
		uint32_t m = mask&-mask;
		mask ^= m;
		letter_masks[plain] = m;

		// next character in cipher
		int i_select1 = selectors[i_select].next[__builtin_ctz(m)];
		solve(cipher, i+1, w, i_select1, location_mask & ~m);
	}
	letter_masks[plain] = letter_mask0;
}
