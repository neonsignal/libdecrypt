// generate ngrams
#include <math.h>
#include <stdint.h>
#include <string>
#include <iostream>
#include <fstream>
typedef unsigned int uint;
const int N = 26;
int main()
{
// initialize ngram array
	static uint ngram[N][N][N][N][N];
	static uint frequency[N];
	static uint8_t table[N][N][N][N][N];
	for (int i0 = 0; i0 < N; ++i0)
	for (int i1 = 0; i1 < N; ++i1)
	for (int i2 = 0; i2 < N; ++i2)
	for (int i3 = 0; i3 < N; ++i3)
	for (int i4 = 0; i4 < N; ++i4)
		ngram[i0][i1][i2][i3][i4] = 0;
	for (int i0 = 0; i0 < N; ++i0)
		frequency[i0] = 0;
// for each dictionary line
	while (!std::cin.eof())
	{
// get count, word, and trailing context
		uint count;
		std::string word, context;
		std::cin >> count >> word >> context;
		//word += 'x'; // letter x for spaces
		int n = word.length();
		word += context;
		for (int i = 0; i < n; ++i)
		{
			int i0 = word[i+0]-'a';
			int i1 = word[i+1]-'a';
			int i2 = word[i+2]-'a';
			int i3 = word[i+3]-'a';
			int i4 = word[i+4]-'a';
			ngram[i0][i1][i2][i3][i4] += count;
			frequency[i0] += count;
		}
	}
/*
// reduce to ngram4
	for (int i1 = 0; i1 < N; ++i1)
	for (int i2 = 0; i2 < N; ++i2)
	for (int i3 = 0; i3 < N; ++i3)
	for (int i4 = 0; i4 < N; ++i4)
	{
		uint cnt = 0;
		for (int i0 = 0; i0 < N; ++i0)
			cnt += ngram[i0][i1][i2][i3][i4];
		for (int i0 = 0; i0 < N; ++i0)
			ngram[i0][i1][i2][i3][i4] = cnt;
	}
*/
// convert to log form
	uint cnt{};
	uint n_max{};
	std::string maxWord;
	for (int i0 = 0; i0 < N; ++i0)
	for (int i1 = 0; i1 < N; ++i1)
	for (int i2 = 0; i2 < N; ++i2)
	for (int i3 = 0; i3 < N; ++i3)
	for (int i4 = 0; i4 < N; ++i4)
		if (ngram[i0][i1][i2][i3][i4] > n_max)
		{
			n_max = ngram[i0][i1][i2][i3][i4];
			std::cerr << (char) (i0+'a') << (char) (i1+'a') << (char) (i2+'a') << (char) (i3+'a') << (char) (i4+'a') << ' ' << int(log(n_max+1)/log(n_max+2)*256) << std::endl;
		}
	for (int i0 = 0; i0 < N; ++i0)
	for (int i1 = 0; i1 < N; ++i1)
	for (int i2 = 0; i2 < N; ++i2)
	for (int i3 = 0; i3 < N; ++i3)
	for (int i4 = 0; i4 < N; ++i4)
	{
		uint n = log(ngram[i0][i1][i2][i3][i4]+1)/log(n_max+1)*255.9;
		table[i0][i1][i2][i3][i4] = n;
		cnt += n;
	}
	std::cerr << "logtotal:" << cnt << ' ' << "tablesize:" << N*N*N*N*N << std::endl;
// write out ngram table
	{
		std::ofstream tableFile("ngram.bin");
		tableFile.write((char *) table, sizeof(table));
		tableFile.close();
	}
// test
	{
		const char m[] = "vakxdtmoxatnzixzyzesyxtedpyfqeooxvvnlanypoyofhgzuwbxvrfafnwokxnastzscbeygzpdtotxcrlkioqkfiewyvtqrojfzxslfszqbqmxbnquruzwnrgcbzkdaysmvrfnbjgezbojgbsbelliiykfirfjwasjurjdvlnalhvhqrbebqgxwltsugvxnrbzgupglrsrkowbehrcdeydaliybbqwzporxdcmekvwyzhhmkgagemryvnepyka";
		int n = sizeof(m)-1;
		uint fit = 0;
		for (int i = 4; i < n; ++i)
			fit += table[m[i-4]-'a'][m[i-3]-'a'][m[i-2]-'a'][m[i-1]-'a'][m[i-0]-'a'];
		std::cerr << "test fit: " << fit << ' ' << n << std::endl;
		for (int i = 0; i < N; ++i)
			std::cerr << (char) ('a'+i) << ' ' << frequency[i] << std::endl;
	}
}
