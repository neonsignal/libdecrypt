// construct a word network
// Copyright Glenn McIntosh 2022
// licensed under the GNU General Public Licence version 3
#pragma once

// include files
#include "codepoints.h"
#include <vector>
#include <memory>

class Wordnet
{
public:
	// add a word to the network
	// word: encoded word
	// i: element to begin from
	void add(const codepoints &word, int i = 0);

	// pseudo-iterators for ranged for
	auto begin() const {return successors.begin();}
	auto end() const {return successors.end();}

	// check if end of word
	bool complete() const {return terminal;}
private:
	// mark end of network
	bool terminal{false};

	// pointers to next elements in network
	struct Successor
	{
		uint8_t code;
		std::unique_ptr<Wordnet> next;
	};
	std::vector<Successor> successors;
};
