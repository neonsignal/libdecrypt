// brute force all members of a set
// Copyright 2023 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#pragma once
#include <array>

// null element
class Brute_null
{
public:
	bool advance()
	{
		return true;
	}
};

// all integers to a limit
// N: number of options
template <int N>
class Brute_number
{
public:
	bool advance() {i = (i+1) % N; return i==0;}
	int operator()() const {return i;}
private:
	int i{};
};

// combine alternate members
template <typename T1, typename T2>
class Brute_alternate
{
public:
	Brute_alternate(T1 &element1, T2 &element2) : element1(element1), element2(element2) {}
	bool advance()
	{
		if (!second)
		{
			if (!element1.advance())
				return false;
		}
		else
		{
			if (!element2.advance())
				return false;
		}
		return second = !second;
	}
	bool operator()() {return second;}
private:
	bool second{true};
	T1 &element1;
	T2 &element2;
};

// combine both members
template <typename T1, typename T2>
class Brute_pair
{
public:
	Brute_pair(T1 &element1, T2 &element2) : element1(element1), element2(element2) {}
	bool advance()
	{
		if (!element1.advance())
			return false;
		if (!element2.advance())
			return false;
		return true;
	}
private:
	T1 &element1;
	T2 &element2;
};
