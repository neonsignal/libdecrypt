// construct a word network
// Copyright Glenn McIntosh 2022
// licensed under the GNU General Public Licence version 3

// include files
#include "wordnet.h"
#include "codepoints.h"
using std::make_unique;

// add a word into the network
void Wordnet::add(const codepoints &word, int i)
{
	// terminal
	if (i == word.size())
	{
		terminal = true;
		return;
	}

	// look for existing prefix
	for (int j = 0; j < ssize(successors); ++j)
		if (successors[j].code == word[i])
		{
			successors[j].next->add(word, i+1);
			return;
		}

	// add to codepoints
	successors.emplace_back(Successor{word[i], make_unique<Wordnet>()});
	successors.back().next->add(word, i+1);
}
