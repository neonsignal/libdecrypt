// permute elements in a sequence
// Copyright 2021 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#pragma once

template <const int n> class Permute
{
// Johnson-Trotter permutation algorithm
// swaps only adjacent elements
#ifdef PERMUTE_BELLCHANGING
public:
	template<typename T> bool operator()(T swap)
	{
		int i{n-1};
		int offset = 0;
		while (pos[i] == (i&dir[i]-1))
		{
			offset += dir[i];
			dir[i] = 1-dir[i];
			if (--i == 0)
				return false;
		}
		pos[i] -= dir[i];
		swap(offset+pos[i]);
		pos[i] += 1-dir[i];
		return true;
	}
private:
	int pos[n]{}, dir[n]{};
#endif
// Heap permutation algorithm
#ifdef PERMUTE_HEAP
public:
	template<typename T> bool operator()(T swap)
	{
		int i{};
		while (pos[i] == i)
		{
			pos[i] = 0;
			if (++i == n)
				return false;
		}
		swap(i, pos[i]&-(i&1));
		++pos[i];
		return true;
	}
private:
	int pos[n]{};
#endif
};
