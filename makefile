# definitions
LIBRARY = libdecrypt
SRC = codepoints wordnet playfair-solve multipleanagram-solve
APP = test main

# compile definitions
CPPFLAGS = -O2 @gcc.conf -MMD -MP -DNDEBUG
LDFLAGS = -O2

# all
all: test

# executables
$(APP): %: %.o $(LIBRARY).a
	$(CXX) $(LDFLAGS) $^ -o $@

# library functions
$(LIBRARY).a: $(SRC:%=%.o)
	rm -f $@ && ar qc $@ $(SRC:%=%.o)

# clean
clean:
	rm -f $(LIBRARY).a *.o

# testing
test.out: test
	./test >/tmp/$$PPID.out && diff -s test.out /tmp/$$PPID.out

#dependencies
-include $(SRC:%=%.d) $(APP:%=%.d)
