// monoalphabetic cipher
// Copyright 2021-2022 Glenn McIntosh
// licensed under the GNU General Public Licence version 3

#pragma once
#include "codepoints.h"
#include <string>

// monoalphabetic substitution
// ciphertext: ciphertext codepoints
// key: key codepoints
// return: plaintext codepoints
codepoints monoalphabetic(const codepoints &ciphertext, const codepoints &key)
{
	codepoints plaintext(ciphertext.size());
	for (int i = 0; i < ciphertext.size(); ++i)
	{
		if (ciphertext[i] < key.size())
			plaintext[i] = key[ciphertext[i]];
		else
			plaintext[i] = ciphertext[i];
	}
	return plaintext;
}

// vignere cipher
// ciphertext: ciphertext codepoints
// key: key codepoints
// return: plaintext codepoints
codepoints vignere(const codepoints &ciphertext, const codepoints &key, const alphabet &alphabet, bool encrypt = false)
{
	codepoints plaintext(ciphertext.size());
	codepoints rkey(key.size());
	for (int i = 0; i < key.size(); ++i)
		rkey[i] = encrypt ? key[i] : alphabet.size()-key[i];
	int j{};
	for (int i = 0; i < ciphertext.size(); ++i)
	{
		if (ciphertext[i] < alphabet.size())
			plaintext[i] = (ciphertext[i] + rkey[j++%key.size()]) % alphabet.size();
		else
			plaintext[i] = ciphertext[i];
	}
	return plaintext;
}
