// multiple anagram decoder
// Copyright 2022 Glenn McIntosh
// licensed under the GNU General Public Licence version 3

#include "codepoints.h"
#include "wordnet.h"
#include <string>
#include <array>
#include <vector>
#include <iostream>

class MultipleAnagramSolver
{
public:
	// construct playfair solver
	MultipleAnagramSolver(std::istream &dictionary);

	// solve ciphertext
	void operator()(const codepoints &cipher, std::vector<codepoints> &solutions);
private:
	// recursive solution
	void solve(int i, const Wordnet &w0, const Wordnet &w1);
private:
	Wordnet wordnet;
	const std::string lc{"abcdefghijklmnopqrstuvwxyz"};

	std::unique_ptr<codepoints> p_solution;
	std::vector<codepoints> *p_solutions;

	std::array<std::array<uint8_t, 26>, 26> pairs;	

	int i_progress{};
};
