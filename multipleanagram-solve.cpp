// multiple anagram solver
// Copyright 2022 Glenn McIntosh
// licensed under the GNU General Public Licence version 3

#include "multipleanagram-solve.h"
using std::ssize;
using std::string;
using std::istream;
using std::make_unique;
using std::vector;
#include <iostream>
using std::cerr, std::endl;

// construct solver
MultipleAnagramSolver::MultipleAnagramSolver(istream &dictionary)
{
	// initialize wordnet
	for (string text{}; getline(dictionary, text);)
		wordnet.add(codepoints(text, lc));
}

// solve ciphertext
void MultipleAnagramSolver::operator()(const codepoints &cipher, vector<codepoints> &solutions)
{
	p_solutions = &solutions;
	if (ssize(cipher)%2)
		return;
	p_solution = make_unique<codepoints>(cipher.size());
	for (int i = 0; i < 25; ++i)
		for (int j = 0; j < 25; ++j)
			pairs[i][j] = 0;
	for (int i = 0; i < cipher.size(); i += 2)
		++pairs[cipher[i+0]][cipher[i+1]];
	solve(0, wordnet, wordnet);
}

// recursive solution
void MultipleAnagramSolver::solve(int i, const Wordnet &w0, const Wordnet &w1)
{
	// log progress
	if (++i_progress % (65536*256) == 0)
	{
		for (int j = 0; j < i; j += 2)
			cerr << char((*p_solution)[j+0]+'a');
		cerr << ' ';	
		for (int j = 0; j < i; j += 2)
			cerr << char((*p_solution)[j+1]+'a');
		cerr << ' ';
		cerr << i << endl;
	}

	// check for solution
	if (i == ssize(*p_solution))
	{
		if (w0.complete() && w1.complete())
		{
			for (int j = 0; j < i; j += 2)
				std::cout << char((*p_solution)[j+0]+'a');
			std::cout << ' ';
			for (int j = 0; j < i; j += 2)
				std::cout << char((*p_solution)[j+1]+'a');
			std::cout << endl;
			p_solutions->push_back(*p_solution);
		}
		return;
	}

	// for each word in wordnet
	if (w0.complete())
		solve(i, wordnet, w1);

	// for each word in wordnet
	else if (w1.complete())
		solve(i, w0, wordnet);

	// for each character pair
	for (auto &s0: w0)
	for (auto &s1: w1)

		// if pair exists
		if (pairs[s0.code][s1.code])
		{
			// add in code pair to solution
			--pairs[s0.code][s1.code];
			(*p_solution)[i+0] = s0.code;
			(*p_solution)[i+1] = s1.code;
			solve(i+2, *(s0.next), *(s1.next));
			++pairs[s0.code][s1.code];
		}
}
