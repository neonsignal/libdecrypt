collection of decryption methods for historical ciphers

Copyright 2017-2022 Glenn McIntosh

licensed under the GNU General Public License version 3, see [LICENSE.md](LICENSE.md)
