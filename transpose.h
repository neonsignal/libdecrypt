// transposition cipher
// Copyright 2022 Glenn McIntosh
// licensed under the GNU General Public Licence version 3

#pragma once
#include "codepoints.h"
#include <algorithm>

// transpose a block
// input: codepoint vector
// step: step size
// reverse: false is a forward transposition, true reverses the columns
// return: codepoint vector
codepoints transpose(codepoints &input, int step, bool reverse = false)
{
	const int n{input.size()};
	codepoints output(input.size());
	if (!reverse)
	{
		int j{0};
		for (int i = 0; i < n-1; ++i)
		{
			output[i] = input[j];
			j = (j+step) % (n-1);
		}
		output[n-1] = input[n-1];
	}
	else
	{
		int j{input.size()};
		for (int i = 0; i < n; ++i)
		{
			j = (j+step) % (n+1);
			output[i] = input[j];
		}
	}
	return output;
}

// test for valid step size
// input: codepoint vector (only size is used)
// step: proposed step size
// reverse: direction of steps
// return: valid step size
bool transpose_test(codepoints input, int step, bool reverse = false)
{
	using std::swap;
	int x0 = input.size(), x1 = step;
	if (!reverse)
		x0 -= 1;
	else
		x0 += 1;
	if (x1 >= x0)
		return false;
	while (x1 > 1)
	{
		x0 = x0%x1;
		swap(x0, x1);
	}
	return x1 == 1;
}
