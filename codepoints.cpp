// define a set of codepoints
// Copyright 2022 Glenn McIntosh
// licensed under the GNU General Public Licence version 3

// include files
#include "codepoints.h"
#include <array>
using std::array;

// index of coincidence
double codepoints::kappa() const
{
	array<int, 256> counts{};
	int total{};
	int actual{}, expected{};
	for (uint8_t d: data)
		if (d != 255)
		{
			actual += counts[d]++*2;
			expected += total++*2;
		}
	return 1.*actual/expected;
}
