// generate ngrams
#include <math.h>
#include <stdint.h>
#include <string>
#include <iostream>
#include <fstream>
typedef unsigned int uint;
const int N = 26;
const int M = 8;
int main()
{
	// initialize ngram array
	static uint ngram[M][N][N];
	static uint8_t table[M][N][N];
	for (int p = 0; p < M; ++p)
	for (int i0 = 0; i0 < N; ++i0)
	for (int i1 = 0; i1 < N; ++i1)
		ngram[p][i0][i1] = 0;

	// for each dictionary line
	while (!std::cin.eof())
	{
		// get count, word, and trailing context
		uint count;
		std::string word, context;
		std::cin >> count >> word >> context;

		int n = word.length();
		int m = context.length();
		if (m > M) m = M;
		word += context;
		for (int i = 0; i < n; ++i)
		{
			int i0 = word[i+0]-'a';
			for (int p = 0; p < m; ++p)
			{
				int i1 = word[i+1+p]-'a';
				ngram[p][i0][i1] += count;
			}
		}
	}

	// convert to log form
	for (int p = 0; p < M; ++p)
	{
		uint cnt{};
		uint n_max{};
		std::string maxWord;
		for (int i0 = 0; i0 < N; ++i0)
		for (int i1 = 0; i1 < N; ++i1)
			if (ngram[p][i0][i1] > n_max)
			{
				n_max = ngram[p][i0][i1];
				std::cerr << (char) (i0+'a') << (char) (i1+'a') << std::endl;
			}
		for (int i0 = 0; i0 < N; ++i0)
		for (int i1 = 0; i1 < N; ++i1)
		{
			uint n = log(ngram[p][i0][i1]+1)/log(n_max+1)*255.9;
			table[p][i0][i1] = n;
			cnt += n;
		}
		std::cerr << "logtotal:" << cnt << ' ' << "tablesize:" << N*N << std::endl;
	}

	// write out ngram table
	{
		std::ofstream tableFile("pairgram.bin");
		tableFile.write((char *) table, sizeof(table));
		tableFile.close();
	}

	// test
	{
		const char m[] = "vakxdtmoxatnzixzyzesyxtedpyfqeooxvvnlanypoyofhgzuwbxvrfafnwokxnastzscbeygzpdtotxcrlkioqkfiewyvtqrojfzxslfszqbqmxbnquruzwnrgcbzkdaysmvrfnbjgezbojgbsbelliiykfirfjwasjurjdvlnalhvhqrbebqgxwltsugvxnrbzgupglrsrkowbehrcdeydaliybbqwzporxdcmekvwyzhhmkgagemryvnepyka";
		int n = sizeof(m)-1;
		uint fit = 0;
		for (int i = 0; i < n; ++i)
			for (int p = 0; p < M && i+1+p < n; ++p)
				fit += table[p][m[i]-'a'][m[i+1+p]-'a'];
		std::cerr << "test fit: " << fit/M << ' ' << n << std::endl;
	}
}
