// ngram fit calculations
#include <fstream>
#include <string>
#include "codepoints.h"
#include <array>

class Ngram
{
	static constexpr int N{26};
public:
	Ngram(const std::string &filename)
	{
		using std::ifstream;
		ifstream file(filename);
		file.read((char *) f, sizeof(f));
		file.close();
	}

	// calculate weighted ngram sum
	// assumes alphabet same as in ngram file
	float fit_weight(const codepoints &lookup)
	{
		const int n{lookup.size()};
		int fit = 0;
		int c0 = 'x'-'a', c1 = 'x'-'a', c2 = 'x'-'a', c3 = 'x'-'a';
		for (int i = 0; i < n; ++i)
		{
			int c4 = lookup[i];
			if (lookup[i] >= N)
				c4 = 'x'-'a';
			fit += f[c0][c1][c2][c3][c4];
			c0 = c1; c1 = c2; c2 = c3; c3 = c4;
		}
		return 1.*fit/n;
	}

	// calculate valid ngram sum
	// assumes alphabet same as in ngram file
	float fit_sequence(const codepoints &lookup)
	{
		const int n{lookup.size()};
		int fit = 0;
		int c0 = 'x'-'a', c1 = 'x'-'a', c2 = 'x'-'a', c3 = 'x'-'a';
		int sequence = 0;
		for (int i = 0; i < n; ++i)
		{
			int c4 = lookup[i];
			if (lookup[i] >= N)
				c4 = 'x'-'a';
			if (f[c0][c1][c2][c3][c4] > 0)
				sequence += 1;
			else
				sequence = 0;
			fit += sequence;
			c0 = c1; c1 = c2; c2 = c3; c3 = c4;
		}
		return fit;
	}

private:
	uint8_t f[N][N][N][N][N];
};

