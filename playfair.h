// playfair cipher
// Copyright 2021-2022 Glenn McIntosh
// licensed under the GNU General Public Licence version 3

#pragma once
#include "codepoints.h"
#include "substitute.h"
#include <algorithm>
#include <array>

class Playfair
{
public:
	// generate playfair tables
	Playfair(bool encrypt=false)
	{
		using std::swap;
		const int rotate{encrypt ? 1 : 4};
		for (int n1 = 0; n1 < 25; ++n1)
			for (int n2 = 0; n2 < 25; ++n2)
			{
				int r1 = n1/5, c1 = n1%5;
				int r2 = n2/5, c2 = n2%5;
				if (r1 == r2 && c1 != c2)
				{
					c1 = (c1+rotate)%5;
					c2 = (c2+rotate)%5;
				}
				else if (c1 == c2 && r1 != r2)
				{
					r1 = (r1+rotate)%5;
					r2 = (r2+rotate)%5;
				}
				else
				{
					swap(c1, c2);
				}
				left[n1][n2] = r1*5+c1;
				right[n1][n2] = r2*5+c2;
		}
	}

	// set key
	// assumes 25 symbol alphabet
	// key: key codepoint vector
	void set_key(const codepoints &key)
	{
		// initialize transposition
		for (int j = 0; j < 25; ++j)
			from_code[j] = 25;
		int i{}, j{}, k{};

		// process key
		while (k < key.size())
		{
			int j{key[k]};
			if (j < 25 && from_code[j] == 25)
			{
				to_code[i] = j;
				from_code[j] = i;
				++i;
			}
			++k;
		}
		// fill in remainder
		while (i < 25)
		{
			while (from_code[j] != 25)
				++j;
			to_code[i] = j;
			from_code[j] = i;
			++i;
		}
	}

	// encrypt/decrypt
	// optionally double characters removed (eg 'X' inserted)
	// assumes padded to even length (eg 'X' padded)
	// assumes 25 symbol alphabet
	// input: input codepoint vector
	// return: output codepoint vector
	codepoints operator()(const codepoints &input)
	{
		int n = input.size();
		codepoints output(n);

		// playfair cipher
		for (int i = 0; i < n; i += 2)
		{
			output[i] = to_code[left[from_code[input[i]]][from_code[input[i+1]]]];
			output[i+1] = to_code[right[from_code[input[i]]][from_code[input[i+1]]]];
		}

		return output;
	}

	// encrypt/decrypt
	// optionally double characters removed (eg 'X' inserted)
	// assumes padded to even length (eg 'X' padded)
	// assumes 25 symbol alphabet
	// input: input codepoint vector
	// tableau: codepoint vector with all symbols required 
	// return: output codepoint vector
	codepoints operator()(const codepoints &input, const codepoints &tableau)
	{
		// process tableau
		for (int k = 0; k <tableau.size(); ++k)
		{
			int j{tableau[k]};
			to_code[k] = j;
			from_code[j] = k;
		}
		return operator()(input);
	}
private:
	std::array<uint8_t, 25> from_code, to_code;
	int left[25][25], right[25][25];
};
